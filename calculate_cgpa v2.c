#include<stdio.h>

int CrHr(int crhr)
{
    int total=0;
    total+=crhr;
    return total;
}

float NumSubCal(int numsub, float cgpa, int crhr)
{
    float calculation;
    calculation = (float)crhr*cgpa*(float)numsub;
    return calculation;
}

float Cgpa(float dem, int nu)
{
    return dem/nu;
}

int main()
{
    int num, crhr, numsub, tcrhr=0;
    float cgpa, cal=0, result=0;

    while(1){
        printf("Cr.Hr. <space> Num. of Sub. <space> GP: ");
        scanf("%d %d %f", &crhr, &numsub, &cgpa);
        num -= numsub;
        tcrhr += CrHr(crhr)*numsub;
        cal += NumSubCal(numsub, cgpa, crhr);
        result = Cgpa(cal, tcrhr);

        printf("Your CGPA: %.2f\n", result);
    }

    return 0;
}
